import sys
import datetime
import argparse
import os
    
#Fonction qui ouvre puis lit en lecture seule le fichier 'data.txt', écrit son contenu dans une liste puis ferme le fichier
def File_Reader():
    Folder = os.path.dirname(os.path.abspath(__file__))
    My_file = os.path.join(Folder, 'data.txt')

    File = open(My_file, "r")
    Content = File.read()
    for Char in Content:
        Complete_list.append(Char)

    File.close()

#Fonction qui lit caractère par caractère le contenu de la liste et sépare chaque ligne dans différentes listes ("Command_list", "Start_Position", "Map_Size")
def Commands_Reader():
    #Ces trois variables ont un fonctionnement de cadena elles se bloquent aux fins de lignes
    global Position 
    global Commands
    global Start
    Index = 0
    
    for Char in Complete_list :
        #Récupère la ligne des commandes "G", "D", "A"
        if  Commands == True and Position == False  and Char != '\n' :
            Command_list.append(Char)
            Index = Index + 1      
        elif Char == '\n' and Commands == True :
            Commands = False
            Index = Index + 1
        #Récupère la ligne pour le première position de la tondeuse    
        if  Position == True and Start == False and Char != '\n' :
            Start_Position.append(Char)
            Index = Index + 1
        elif Char == '\n' and Position == True :
            Position = False
            Commands = True
            Index = Index + 1
        #Récupère la ligne pour la taille de la pelouse
        if Start == True and Char != '\n' :
            Map_Size.append(Char)
            Index = Index + 1
        elif Char == '\n' and Start == True :
            Start = False
            Position = True
            Index = Index + 1
    #Boucle permettant de supprimer le contenu de la liste complète qui a été récupéré juste avant dans les autres listes
    Del_Char = 0
    while Del_Char < Index :
        Del_Char = Del_Char + 1
        Complete_list.pop(0)
    
    return Map_Size, Start_Position, Command_list

#Rempli la liste multidimensionnelle du caractère "G" qui signifie simplement Green
def Map_Display():
    global Map_Tab
    Map_Tab = [] 
    for Index2 in range(rows):
        Map_Tab.append(["G"] * cols) 
         
#Fonction de placement de la tondeuse au départ et gestion si une nouvelle tondeuse apparait disparition de l'ancienne
def Mower_Place():
    global Direction 
    
    for Line in range(rows):
            for Columns in range(cols):
                Color = Map_Tab[Line][Columns]
                if Color == "T":
                    Map_Tab[Line][Columns] = "B"

    Map_Tab[int(Start_Position[0])][int(Start_Position[1])] = "T"
    #Gestion de la direction au placement de la tondeuse selon le point cardinal où elle est dirigée
    if Start_Position[3] == "N" :
        Direction = 0
    elif Start_Position[3] == "S" :
        Direction = 180
    elif Start_Position[3] == "E" :
        Direction = 90
    elif Start_Position[3] == "W" :
        Direction = 270
    
    Display()
    Commands_Decrypt(Line, Columns)
#Fonction de décryptage des commandes situées dans la liste "Command_list"
def Commands_Decrypt(Line, Columns):
    Mower = False
    global Direction 
    Final_Direction = ""
    #Parcours de la liste des commandes en gérant les différentes options possibles entre gauche, droite ou Avancer selon la direction de la tondeuse
    for Char in Command_list :

        if Char == "G" :
            if Direction == 0 :
                Direction = 270
            elif Direction != 0 :
                Direction = Direction - 90
        elif Char == "D" : 
            if Direction == 270 :
                Direction = 0
            elif Direction != 270: 
                Direction = Direction + 90

        if Char == "A" :   
            if Direction == 0 :
                if int(Start_Position[1]) + 1 !=  cols :
                    Change_Color_Cell(Map_Tab[int(Start_Position[0])][int(Start_Position[1])], Mower)
                    Start_Position[1] = int(Start_Position[1]) + 1
                    Mower = True
                    Change_Color_Cell(Map_Tab[int(Start_Position[0])][int(Start_Position[1])], Mower)
                    Final_Direction = "N"
            
            if Direction == 180 :
                if int(Start_Position[1]) - 1 >=  0:
                    Change_Color_Cell(Map_Tab[int(Start_Position[0])][int(Start_Position[1])], Mower)
                    Start_Position[1] = int(Start_Position[1]) - 1
                    Mower = True
                    Change_Color_Cell(Map_Tab[int(Start_Position[0])][int(Start_Position[1])], Mower)
                    Final_Direction = "S"
            
            if Direction == 90 :
                if int(Start_Position[0]) + 1 <  rows:
                    Change_Color_Cell(Map_Tab[int(Start_Position[0])][int(Start_Position[1])], Mower)
                    Start_Position[0] = int(Start_Position[0]) + 1
                    Mower = True
                    Change_Color_Cell(Map_Tab[int(Start_Position[0])][int(Start_Position[1])], Mower)
                    Final_Direction = "E"

            if Direction == 270 :
                if int(Start_Position[0]) - 1 >= 0 :
                    Change_Color_Cell(Map_Tab[int(Start_Position[0])][int(Start_Position[1])], Mower)
                    Start_Position[0] = int(Start_Position[0]) - 1
                    Mower = True
                    Change_Color_Cell(Map_Tab[int(Start_Position[0])][int(Start_Position[1])], Mower)
                    Final_Direction = "W"
       
        Display()
    #Affichage de la dernière position de la tondeuse et sa direction
    print("La tondeuse finie en  :" , Start_Position[0] , Start_Position[1] , Final_Direction )

def Display():
    #Affichage dans le terminal étape par étape l'avancement de la tondeuse
    print("Début de mouvement : ") 
    for Line in range(rows-1, -1, -1):
        for Columns in range(cols):
            print("" ,Map_Tab[Line][Columns], end='')      
        print("\n")
    print("Fin de mouvement. ")
    print("\n")

#Fonction de changement d'état d'une cellule de "T = Tondeuse" en "B = Brown" et "G = Green" ou "B" en "T" si une tondeuse passe dessus
def Change_Color_Cell(Actual_Mower_Place, Mower):
    Color_actuelle = Map_Tab[int(Start_Position[0])][int(Start_Position[1])]
    
    if Color_actuelle == "T":
        New_Color = "B"

    elif Color_actuelle == "G" or Mower and Color_actuelle == "B": 
        New_Color = "T"
    
    if Color_actuelle != New_Color:
        Map_Tab[int(Start_Position[0])][int(Start_Position[1])] = New_Color     
        
#Création d'un main qui crée la variables de base du programme puis lance les premières fonctions
if __name__ == '__main__':
    
    global Actual_Mower_Place

    Actual_Mower_Place = []
    Map_Size = list()
    Start_Position = list()
    Command_list = list()
    Complete_list = list()
    File_Reader()
        
    Start = True
    Position = False
    Commands = False
    Commands_Reader()

    rows = int(Map_Size[0]) + 1
    cols = int(Map_Size[1]) + 1    

    Map_Display()
    
    Mower_Place()
    #Boucle while qui regarde si la liste complète est vide si elle  ne l'est pas il vide les listes et relance les fonctions jusqu'à vider la liste
    while len(Complete_list) > 0 :
        print("NOUVELLE TONDEUSE") 
        Position = True
        Commands = False
        Start_Position.clear()
        Command_list.clear()
        Commands_Reader()
        Mower_Place()     

